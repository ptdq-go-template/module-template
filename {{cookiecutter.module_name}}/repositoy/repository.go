package repositoy

import (
	"gitlab.com/phongthien/repository/repository"
	"go.mongodb.org/mongo-driver/mongo"
	"{{cookiecutter.module_path}}/{{cookiecutter.module_name}}/model"
)

type IHistorySignalRepository interface {
	repository.IRepository[model.Example]
}
type HistoryRepository struct {
	repository.BaseMongoRepository[model.Example]
}

func NewHistorySignalRepository(database *mongo.Database) IHistorySignalRepository {
	return &HistoryRepository{
		BaseMongoRepository: repository.BaseMongoRepository[model.Example]{
			Collection: database.Collection("example"),
		},
	}

}
