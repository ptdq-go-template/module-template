package controller

import "go.uber.org/zap"

type IController interface {
}
type controller struct {
	logger *zap.SugaredLogger
}

func NewController(logger *zap.SugaredLogger) IController {
	return &controller{
		logger: logger,
	}
}
