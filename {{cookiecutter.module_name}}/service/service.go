package service

import "go.uber.org/zap"

type IService interface {
}
type service struct {
	logger *zap.SugaredLogger
}

func NewService(logger *zap.SugaredLogger) IService {
	return &service{
		logger: logger,
	}
}
