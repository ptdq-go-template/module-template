package {{cookiecutter.module_name}}

import "go.uber.org/fx"

var token = "{{cookiecutter.module_name}}"
var Module = fx.Module(token)
